<?php

session_start();

include('db.php');

if(isset($_GET['id_produit'])) { 
    
    $select_produits = $bdd->prepare('SELECT * FROM produits WHERE proprietaire_produit = ? AND id = ?');
    $select_produits->execute(array($_SESSION['id'], $_GET['id_produit']));
    $produit = $select_produits->fetch();
    
    if(isset($_POST['valider'])) {

        if($_POST['nom_produit'] != $produit['nom_produit']) {

            $update_nom_produit = $bdd->prepare('UPDATE produits SET nom_produit = ? WHERE proprietaire_produit = ? AND id = ?');
            $update_nom_produit->execute(array($_POST['nom_produit'], $_SESSION['id'], $_GET['id_produit']));

        }

        if($_POST['desc_produit'] != $produit['description_produit']) {

            $update_desc_produit = $bdd->prepare('UPDATE produits SET description_produit = ? WHERE proprietaire_produit = ? AND id = ?');
            $update_desc_produit->execute(array($_POST['desc_produit'], $_SESSION['id'], $_GET['id_produit']));

        }

        if($_POST['prix_produit'] != $produit['prix_produit']) {

            $update_prix_produit = $bdd->prepare('UPDATE produits SET prix_produit = ? WHERE proprietaire_produit = ? AND id = ?');
            $update_prix_produit->execute(array($_POST['prix_produit'], $_SESSION['id'], $_GET['id_produit']));

        }

        header('Location: mes_produits.php');

    }
    if(isset($_POST['suppr'])) {

        $nom_image = $produit['id_image'];
        $extension_image = $produit['extension_image'];
        $fichier = "images/{$nom_image}{$extension_image}";
        unlink($fichier);

        $delete_produit = $bdd->prepare('DELETE FROM produits WHERE id = ?');
        $delete_produit->execute(array($produit['id']));

        header("Location: mes_produits.php");

    }
    
    
    ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <form method="POST">
            <img style="width:20%; height:20%; postion:relative;"
                src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>" alt=""> <br>
            <label>Nom produit :</label>
            <input type="text" name="nom_produit" value="<?php echo $produit['nom_produit']; ?>">
            <label>Descriptiondu produit :</label>
            <textarea name="desc_produit" cols="30" rows="10"><?php echo $produit['description_produit']; ?></textarea>
            <label>Prix du produit :</label>
            <input type="text" name="prix_produit" value="<?php echo $produit['prix_produit']; ?>">
            <input type="submit" value="Valider" name="valider">
            <input type="submit" value="Supprimer" name="suppr">
        </form>
    </div>

    <?php
} else {

    ?>
    <link rel="stylesheet" href="css/mes_produits.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div>
        <ul class="topnav">
            <a href="index.php"><img src="images_static/logo_market.webp" class="logo_market"></a>
            <a href="index.php">
                <h3 class="marque">RansomCa$h</h3>
            </a> <?php
            if($_SESSION['id']) { ?>

            <li class="Tosell"><a href="c_produit.php">+ Nouveau Produit </a></li>
            <li class="Tosell2"><a href="mes_produits.php">Mes produits</a></li>
            <li><a href="panier.php">Panier <?php echo $nombre_produit_user; ?></a></li>
            <li><a href="liste_discussions.php">Discutions</a></li>

            <?php 
            }
                if($_SESSION['id']) { 

                    $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                    $select_username->execute(array($_SESSION['id']));
                    $user = $select_username->fetch();?>

            <li><img src="images_static/connexion.png"
                    style="margin-left: -2%; padding-right: 0; margin-top: 0.5%; position: absolute; height: 25px; width: 42px;"><a
                    href="index.php?deco"><?php echo $user['username']; ?></a>
            </li>
            <?php
                } else { ?>
            <li><a href="connexion.php">Connexion</a></li>
            <li><a href="inscription.php">Inscription</a></li>
            <?php
                } ?>
        </ul>
    </div>
    <div> <?php
            $select_produits = $bdd->prepare('SELECT * FROM produits WHERE proprietaire_produit = ?');
            $select_produits->execute(array($_SESSION['id']));
            $nbr_produits = $select_produits->rowCount();

            if($nbr_produits >= 1) {
                while($produit = $select_produits->fetch()) { ?>

        <div>
            <form method="GET">
                <a style="text-decoration:none; color:black;"
                    href="mes_produits.php?id_produit=<?php echo $produit['id']; ?>">
                    <img style="width:20%; height:20%; postion:relative;"
                        src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>"
                        alt="">
                    <p><?php echo $produit['nom_produit']; ?> &nbsp; <?php echo $produit['prix_produit']; ?>€</p>
                </a>
            </form>
        </div>
        <br><br>

        <?php
                }
            } else {
                echo"Vous n'avez aucin produits pour le moment";
            }
            ?>
    </div>
    <?php
}
?>