<?php

session_start();

include('db.php');


if(isset($_POST['valider'])) {

    if(!empty($_POST['username']) AND !empty($_POST['password']) AND !empty($_POST['password_conf'])) {

        $verif_username = $bdd->prepare('SELECT username FROM users WHERE username = ?');
        $verif_username->execute(array($_POST['username']));
        $username_exist = $verif_username->rowCount();

        if($username_exist === 0) {

                if($_POST['password'] == $_POST['password_conf']) {

                    $password_crypted = password_hash($_POST['password'], PASSWORD_BCRYPT);

                    $insertion_utilisateur = $bdd->prepare("INSERT INTO users (username, mdp, administrateur) VALUES (?,?,?)");
                    $insertion_utilisateur->execute(array($_POST['username'], $password_crypted, '0'));

                    header('Location: connexion.php');

                } else {
                    $message = "les mots de passe correspondent pas";
                }

        } else {
            $message = "Username deja utilise";
        }
    } else {
        $message = "non c pas bon";
    }

}

?>
<link rel="stylesheet" href="css/index.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div>
    <ul class="topnav">
        <a href="index.php"><img src="images_static/logo_market.webp" class="logo_market"></a>
        <a href="index.php">
            <h3 class="marque">RansomCa$h</h3>
        </a> <?php
            if($_SESSION['id']) { ?>

        <li class="Tosell"><a href="c_produit.php">+ Nouveau Produit </a></li>
        <li class="Tosell2"><a href="mes_produits.php">Mes produits</a></li>
        <li><a href="panier.php">Panier <?php echo $nombre_produit_user; ?></a></li>
        <li><a href="liste_discussions.php">Discutions</a></li>

        <?php 
            }
                if($_SESSION['id']) { 

                    $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                    $select_username->execute(array($_SESSION['id']));
                    $user = $select_username->fetch();?>

        <li><img src="images_static/connexion.png"
                style="margin-left: -2%; padding-right: 0; margin-top: 0.5%; position: absolute; height: 25px; width: 42px;"><a
                href="index.php?deco"><?php echo $user['username']; ?></a>
        </li>
        <?php
                } else { ?>
        <li><a href="connexion.php">Connexion</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <?php
                } ?>
    </ul>
</div>

<div>
    <form method="POST">
        <input type="text" placeholder="Username" name="username">
        <input type="password" placeholder="Password" name="password">
        <input type="password" placeholder="Confirmation password" name="password_conf">
        <input type="submit" value="Valider" name="valider">
    </form>
    <?php

        if(isset($message)) { ?>

    <p><?php echo $message;?></p>

    <?php
        }

        ?>
</div>
</body>

</html>