<?php

session_start();

include('db.php');

if(isset($_GET['accueil'])) { 
    
    $verif_id = $bdd->prepare('SELECT * FROM users WHERE id = ?');
    $verif_id->execute(array($_GET['accueil']));
    $user = $verif_id->fetch();
    $id_exist = $verif_id->rowCount();

    if($id_exist === 1) {

        if($user['administrateur'] == 1) {

            if(isset($_POST['deco'])) {

                session_destroy();
                header('Location: index.php');
            
            }
    
            ?>

            <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Admin Accueil</title>
            </head>
            <body>
                <form method="POST">
                    <?php
                    if($_SESSION['id']) { 
                        $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                        $select_username->execute(array($_SESSION['id']));
                        $user = $select_username->fetch();?>

                        <input type="submit" name="deco" value="<?php echo $user['username']; ?>">
                        <?php
                    } else {?>
                        <input type="submit" name="connexion" value="Connexion">
                        <input type="submit" name="inscription" value="Inscription">
                    <?php
                   }?>
                </form><?php
                $select_demande_approuve = $bdd->query('SELECT * FROM produits WHERE approuve = 0');
                $demande_approuve = $select_demande_approuve->rowCount(); ?>
                <a href="liste_demande_approuve.php">Demande(s) en attente: <?php echo $demande_approuve; ?></a> <br>
                <a href="c_produit.php">Ajouter un produit</a>
                <div>
                    <label>Liste des produits :</label>
                </div>
                <?php
                $select_produits = $bdd->query('SELECT * FROM produits WHERE id > 0');

                while($produit = $select_produits->fetch()) { ?>

                    <div>
                        <form method="GET">
                            <a style="text-decoration:none; color:black;" href="index.php?id_produit=<?php echo $produit['id_produit']; ?>">
                                <img style="width:20%; height:20%; postion:relative;" src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>" alt="">
                                <p><?php echo $produit['nom_produit']; ?> &nbsp; <?php echo $produit['prix_produit']; ?>€</p>
                            </a>
                        </form>
                    </div>
                    <br><br>

                <?php
                }
                ?>
            </body>
            </html> 
            <?php
        } else {
            echo "vous n'êtes pas administrateur";
        }
    } else {
        echo "identifiant invalide";
    }
} else {

    if(isset($_POST['valider'])) {

        if(!empty($_POST['username']) AND !empty($_POST['password'])) {

            $verif_username = $bdd->prepare('SELECT * FROM users WHERE username = ?');
            $verif_username->execute(array($_POST['username']));
            $user = $verif_username->fetch();
            $username_exist = $verif_username->rowCount();

            if($username_exist === 1) {

                if($user['administrateur'] == 1) {

                    if(password_verify($_POST['password'], $user['mdp'])) {

                        $_SESSION['id'] = $user['id'];

                        header('Location: index.php');

                    } else {
                        echo "mauvais password";
                    }
                } else {
                    echo "pas admin";
                }
            }
        }
    }

    if(isset($_SESSION['id'])) {

        header("Location: admin.php?accueil=" .$_SESSION['id']);

    } else { ?>

        <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Connexion</title>
    </head>
    <body>
        <div>
            <form method="POST">
                <input type="text" placeholder="Username" name="username">
                <input type="password" placeholder="Password" name="password">
                <input type="submit" value="Valider" name="valider">
            </form>
        </div>
    </body>
    </html>
    <?php
}
}