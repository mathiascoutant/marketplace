<?php

session_start();

include('db.php');

if($_SESSION['id']) {

    if(!empty($_GET['id_produit'])) {

        $select_produit = $bdd->prepare('SELECT * FROM produits WHERE id_produit = ?');
        $select_produit->execute(array($_GET['id_produit']));
        $produit = $select_produit->fetch();
        $produit_exist = $select_produit->rowCount();

        if($produit_exist === 1) {


            if(isset($_POST['valider'])){

                if(!empty($_POST['nom_produit']) AND !empty($_POST['description_produit']) AND !empty($_POST['prix_produit'])) {



                    if($produit['nom_produit'] != $_POST['nom_produit']) {

                        $update_nom_produit = $bdd->prepare('UPDATE produits SET nom_produit = ? WHERE id_produit = ?');
                        $update_nom_produit->execute(array($_POST['nom_produit'], $_GET['id_produit']));

                    }

                    if($produit['description_produit'] != $_POST['description_produit']) {

                        $update_description_produit = $bdd->prepare('UPDATE produits SET description_produit = ? WHERE id_produit = ?');
                        $update_description_produit->execute(array($_POST['description_produit'], $_GET['id_produit']));

                    }

                    if($produit['prix_produit'] != $_POST['prix_produit']) {

                        $update_prix_produit = $bdd->prepare('UPDATE produits SET prix_produit = ? WHERE id_produit = ?');
                        $update_prix_produit->execute(array($_POST['prix_produit'], $_GET['id_produit']));

                    }

                    header("Location: modification_produit.php?id_produit=" .$produit['id_produit']);

                } else {
                    echo"Tout doit etre renseigné";
                }

            }


            ?>

            <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Modification-Produit</title>
            </head>
            <body>
                <div>
                    <form method="post">
                        <input type="text" value="<?php echo $produit['nom_produit']; ?>" name="nom_produit">
                        <input type="text" value="<?php echo $produit['description_produit']; ?>" name="description_produit">
                        <input type="text" value="<?php echo $produit['prix_produit']; ?>" name="prix_produit">
                        <input type="submit" value="Valider" name="valider">
                    </form>
                </div>
            </body>
            </html>

        <?php
        }
    }
}
?>
