<?php

session_start();

include('db.php');

if($_SESSION['id']) {

    $key = "tp";

    if(isset($_GET['user_id'])) {

        $verif_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
        $verif_username->execute(array($_GET['user_id']));
        $user = $verif_username->fetch();
        $user_exist = $verif_username->rowCount();

        if($user_exist == 1) {

            if($_SESSION['id'] != $_GET['user_id']) {

                    if(isset($_POST['envoyer'])) {

                        if(!empty($_POST['message'])) {

                            $date = date("d.m.y");

                            $messge = openssl_encrypt($_POST['message'], "AES-128-ECB", $key); 

                            $insertion_message = $bdd->prepare("INSERT INTO discussions (user_env, user_rec, date, message) VALUES (?,?,?,?)");
                            $insertion_message->execute(array($_SESSION['id'], $user['id'], $date, $messge));

                            header("Location: discussions.php?user_id=" .$_GET['user_id']);


                        }

                    }

                    if(isset($_POST['bloquer'])) {

                        $date = date("d.m.y");

                        $insertion_bloquage = $bdd->prepare("INSERT INTO users_bloque (user_demande_bloque, user_bloque, date) VALUES (?,?,?)");
                        $insertion_bloquage->execute(array($_SESSION['id'], $user['id'], $date));

                        header("Location: discussions.php?user_id=" .$_GET['user_id']);

                    }

                    if(isset($_POST['debloquer'])) {

                        $date = date("d.m.y");

                        $insertion_bloquage = $bdd->prepare('DELETE FROM users_bloque where user_bloque = ? AND user_demande_bloque = ?');
                        $insertion_bloquage->execute(array($user['id'], $_SESSION['id']));

                        header("Location: discussions.php?user_id=" .$_GET['user_id']);

                    }


                    ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Discussion avec <?php echo $user['username'];?></title>
</head>

<body>
    <div>
        <?php
                            $verif_username_bloque = $bdd->prepare('SELECT * FROM users_bloque WHERE user_bloque = ? AND user_demande_bloque = ?');
                            $verif_username_bloque->execute(array($_GET['user_id'], $_SESSION['id']));
                            $user_is_bloque = $verif_username_bloque->rowCount();
                            
                            if($user_is_bloque === 0) { ?>
        <form method="post">
            <input type="submit" name="bloquer" value="Bloquer">
        </form><?php
                            } else { ?>
        <form method="post">
            <input type="submit" name="debloquer" value="Débloquer">
        </form><?php
                            } ?>
    </div>
    <div style="height=30%;"> <?php
                            $select_messages = $bdd->prepare('SELECT * FROM discussions WHERE user_env = ? AND user_rec = ? OR user_env = ? AND user_rec = ?');
                            $select_messages->execute(array($_SESSION['id'], $user['id'], $user['id'], $_SESSION['id']));
                            $nbr_message = $select_messages->rowCount();

                            if($nbr_message > 0) {

                                while($message = $select_messages->fetch()) { 

                                    $verif_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                                    $verif_username->execute(array($message['user_env']));
                                    $user = $verif_username->fetch(); 
                                    $decrypted_chaine = openssl_decrypt($message['message'], "AES-128-ECB", $key);?>

        <p></p>
        <p><?php echo $user['username']; ?> : &nbsp; <?php echo $decrypted_chaine; ?></p>

        <?php
                                }
                            } else { ?>
        <p>Début de la discussion avec <?php echo $user['username'];?></p> <?php
                            }
                            ?>
    </div>
    <?php  
                        $verif_username_bloque = $bdd->prepare('SELECT * FROM users_bloque WHERE user_bloque = ? AND user_demande_bloque = ?');
                        $verif_username_bloque->execute(array($_SESSION['id'], $_GET['user_id']));
                        $user_is_bloque = $verif_username_bloque->rowCount();

                        $verif_username_bloque = $bdd->prepare('SELECT * FROM users_bloque WHERE user_bloque = ? AND user_demande_bloque = ?');
                        $verif_username_bloque->execute(array($_GET['user_id'], $_SESSION['id']));
                        $user_bloque = $verif_username_bloque->rowCount();

                        if($user_is_bloque == 0 AND $user_bloque === 0) { ?>
    <div>
        <form method="post" style="position:absolute; margin-top:49%;">
            <textarea name="message" cols="195" rows="2" placeholder="Entrez votre message..."></textarea>
            <input type="submit" name="envoyer">
        </form>
    </div> <?php
                        } else { ?>
    <form method="post" style="position:absolute; margin-top:49%;">
        <p>Vous etes bloquer par cette personne ou vous avez bloquer cette personne !</p>
    </form> <?php
                        } ?>
</body>

</html>

<?php
            } else {
                echo "vous ne pouvez pas vous envoyer de message a vous meme mdrr";
            } 
        } else {
            echo "cette personne n'existe pas !";
        }

    } else {
        echo "personne pas trouvé !";
    }

} else {
    header('Location: index.php');
}
?>