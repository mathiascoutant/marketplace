<?php

session_start();

include('db.php');

if(isset($_GET['approuve'])){

    $verif_id = $bdd->prepare('SELECT * FROM produits WHERE id = ?');
    $verif_id->execute(array($_GET['approuve']));
    $user = $verif_id->fetch();
    $id_exist = $verif_id->rowCount();

    if($id_exist === 1) {

        $update_id = $bdd->prepare('UPDATE produits SET approuve = 1 WHERE id = ?');
        $update_id->execute(array($_GET['approuve']));

        header("Location: liste_demande_approuve.php");

    } else {
        echo"produit inexistant";
    }
}

if(isset($_GET['decline'])){

    $verif_id = $bdd->prepare('SELECT * FROM produits WHERE id = ?');
    $verif_id->execute(array($_GET['decline']));
    $produit = $verif_id->fetch();
    $id_exist = $verif_id->rowCount();

    if($id_exist === 1) {

        $nom_image = $produit['id_image'];
        $extension_image = $produit['extension_image'];
        $fichier = "images/{$nom_image}{$extension_image}";
        unlink($fichier);

        $delete_produit = $bdd->prepare('DELETE FROM produits WHERE id = ?');
        $delete_produit->execute(array($_GET['decline']));

        header("Location: liste_demande_approuve.php");

    } else {
        echo"produit inexistant";
    }
}

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste demande Approuve</title>
</head>
<body>
    <div>
    <?php
        $select_produits = $bdd->query('SELECT * FROM produits WHERE approuve = 0');
        $nbr_demande = $select_produits->rowCount();

        if($nbr_demande >= 1) {
            while($produit = $select_produits->fetch()) { ?>

                <div>
                    <form method="GET">
                        <a style="text-decoration:none; color:black;" href="index.php?id_produit=<?php echo $produit['id_produit']; ?>">
                            <img style="width:20%; height:20%; postion:relative;" src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>" alt="">
                            <p><?php echo $produit['nom_produit']; ?> &nbsp; <?php echo $produit['prix_produit']; ?>€</p>
                        </a>
                        <a href="liste_demande_approuve.php?approuve=<?php echo $produit['id']; ?>">Approuver</a>
                        <a href="liste_demande_approuve.php?decline=<?php echo $produit['id']; ?>">Décliner</a>
                    </form>
                </div>
                <br><br>

            <?php
            }
        } else {
            echo"aucune demande pour le moment";
        }
        ?>
    </div>
</body>
</html>