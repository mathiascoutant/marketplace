<?php

session_start();

include('db.php');

if(isset($_GET['deco'])) {

    session_destroy();
    header('Location: index.php');

}

if(isset($_POST['mes_produits'])) {

    header('Location: mes_produits.php');

}
$select_nombre_produit = $bdd->prepare('SELECT proprietaire FROM panier WHERE proprietaire = ?');
$select_nombre_produit->execute(array($_SESSION['id']));
$nombre_produit_user = $select_nombre_produit->rowCount();
 
if(empty($_GET['id_produit'])) { ?>

<link rel="stylesheet" href="css/index.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div>
    <ul class="topnav">
        <a href="index.php"><img src="images_static/logo_market.webp" class="logo_market"></a>
        <a href="index.php">
            <h3 class="marque">RansomCa$h</h3>
        </a> <?php
            if($_SESSION['id']) { ?>

        <li class="Tosell"><a href="c_produit.php">+ Nouveau Produit </a></li>
        <li class="Tosell2"><a href="mes_produits.php">Mes produits</a></li>
        <li><a href="panier.php">Panier <?php echo $nombre_produit_user; ?></a></li>
        <li><a href="liste_discussions.php">Discutions</a></li>

        <?php 
            }
                if($_SESSION['id']) { 

                    $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                    $select_username->execute(array($_SESSION['id']));
                    $user = $select_username->fetch();?>

        <li><img src="images_static/connexion.png"
                style="margin-left: -2%; padding-right: 0; margin-top: 0.5%; position: absolute; height: 25px; width: 42px;"><a
                href="index.php?deco"><?php echo $user['username']; ?></a>
        </li>
        <?php
                } else { ?>
        <li><a href="connexion.php">Connexion</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <?php
                } ?>
    </ul>
</div>

<?php

    $select_produits = $bdd->query('SELECT * FROM produits WHERE id > 0 AND approuve = 1');

    while($produit = $select_produits->fetch()) { ?>

<div class="produits">
    <form method="GET">
        <a class="bloc" style="text-decoration:none; color:black;"
            href="index.php?id_produit=<?php echo $produit['id_produit']; ?>">
            <img style="margin-top: 5%; width: 80%; height: 80%; postion: absolute; margin-left: 10%;"
                src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>" alt="">
            <p class="p"><?php echo $produit['nom_produit']; ?> &nbsp; <?php echo $produit['prix_produit']; ?>€</p>
        </a>
    </form>
</div>


<?php
    }
} else {

    $select_produit = $bdd->prepare('SELECT * FROM produits WHERE id_produit = ?');
    $select_produit->execute(array($_GET['id_produit']));
    $produit = $select_produit->fetch();
    $is_exist = $select_produit->rowCount();

    if($_SESSION['id']) {

        $select_nombre_produit = $bdd->prepare('SELECT proprietaire FROM panier WHERE proprietaire = ?');
        $select_nombre_produit->execute(array($_SESSION['id']));
        $nombre_produit_user = $select_nombre_produit->rowCount();

        if(isset($_POST['panier'])) {

            header('Location: panier.php');

        }

    }

    if(isset($_POST['ajout_panier'])) {

        if($_SESSION['id']) {

            $select_produit_panier = $bdd->prepare('SELECT * FROM panier WHERE id_produit = ? AND proprietaire = ?');
            $select_produit_panier->execute(array($_GET['id_produit'], $_SESSION['id']));
            $produits = $select_produit_panier->fetch();
            $is_exist_panier = $select_produit_panier->rowCount();

            if($is_exist_panier === 0) {

                $insertion_produit_panier = $bdd->prepare("INSERT INTO panier (proprietaire, nom_produit, id_produit) VALUES (?,?,?)");
                $insertion_produit_panier->execute(array($_SESSION['id'], $produit['nom_produit'], $produit['id_produit']));

                header("Location: index.php?id_produit=".$produit['id_produit']);

            } else {
                echo "deja ajout panier";
            }

        } else {
            echo "pas connecter";
        }

    }

    if(isset($_POST['panier'])) {

        if($_SESSION['id']) {

            header('Location: panier.php');

        } else {
            echo "pas connecter";
        }
    }


    if($is_exist === 1) { 
        
        if($_SESSION['id']) { ?>

<link rel="stylesheet" href="css/index.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div>
    <ul class="topnav">
        <a href="index.php"><img src="images_static/logo_market.webp" class="logo_market"></a>
        <a href="index.php">
            <h3 class="marque">RansomCa$h</h3>
        </a>
        <li class="Tosell"><a href="c_produit.php">+ Nouveau Produit </a></li>
        <li class="Tosell2"><a href="mes_produits.php">Mes produits</a></li>
        <li><a href="panier.php">Panier <?php echo $nombre_produit_user; ?></a></li>
        <li><a href="liste_discussions.php">Discutions</a></li>
        <?php
            if($_SESSION['id']) { 

                $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                $select_username->execute(array($_SESSION['id']));
                $user = $select_username->fetch();?>

        <li><a href="index.php?deco"><?php echo $user['username']; ?></a></li>
        <?php
            } else { ?>
        <li><a href="connexion.php">Connexion</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <?php
            } ?>
    </ul>
</div>

<div>
    <form method="GET">
        <a style="text-decoration:none; color:black;" href="index.php?id_produit=<?php echo $produit['id_produit']; ?>">
            <img style="width:20%; height:20%; postion:relative;"
                src="images/<?php echo $produit['id_image']; ?><?php echo $produit['extension_image']; ?>" alt="">
            <p><?php echo $produit['nom_produit']; ?> &nbsp; <?php echo $produit['prix_produit']; ?>€</p>
            <p><?php echo $produit['description_produit']; ?></p>
        </a>

    </form>
    <form method="POST">
        <input type="submit" value="Ajout au panier" name="ajout_panier">
    </form>
    <div>
        <a href="discussions.php?user_id=<?php echo $produit['proprietaire_produit'] ;?>">Discuter avec le
            propriétaire</a>
    </div>
</div> <?php
        } else {
            echo "vous devez etre connecter pour pouvooir voir plus en detail le produit";
        }

    } else {
        echo "Le produit n existe pas";
    }

}


?>