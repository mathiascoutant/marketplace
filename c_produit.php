<?php


session_start();

include('db.php');


if(!empty($_SESSION['id'])) {

        if(isset($_POST['valider'])) {
            if(!empty($_POST['nom_produit']) AND !empty($_POST['desc_produit']) AND !empty($_POST['prix_produit'])) {

                if($_SERVER['REQUEST_METHOD'] === 'POST') {

                    if(isset($_FILES['image']) AND !empty($_FILES['image']['name'])) {
                        $id_produit = uniqid();
                        foreach ($_FILES['image']['name'] as $key => $name) {

                            $chaine = uniqid();
                            $extension_upload = strrchr($_FILES['image']['name'][$key], '.');
                            $nom = "images/{$chaine}{$extension_upload}";
                            $resultat = move_uploaded_file($_FILES['image']['tmp_name'][$key],$nom);

                            $initialisation_format_jour = 'd-m-Y';
                            $date = date($initialisation_format_jour);

                            $insertion_produit = $bdd->prepare("INSERT INTO produits (nom_produit, description_produit, proprietaire_produit, date_produit, prix_produit, id_produit, id_image, extension_image, approuve) VALUES (?,?,?,?,?,?,?,?,?)");
                            $insertion_produit->execute(array($_POST['nom_produit'], $_POST['desc_produit'] , $_SESSION['id'] , $date, $_POST['prix_produit'], $id_produit, $chaine, $extension_upload, '0'));


                            if ($resultat){
                                echo "image ok";
                            }
                        }
                    } else {
                        echo "non";
                    }
                }
                echo"votre demande a bien ete prise en compte";

            } else {
                echo "vous devez rentrer toutes les informations";
            }
        }
        ?>
<link rel="stylesheet" href="css/nouveau_produit.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div>
    <ul class="topnav">
        <a href="index.php"><img src="images_static/logo_market.webp" class="logo_market"></a>
        <a href="index.php">
            <h3 class="marque">RansomCa$h</h3>
        </a> <?php
            if($_SESSION['id']) { ?>

        <li class="Tosell"><a href="c_produit.php">+ Nouveau Produit </a></li>
        <li class="Tosell2"><a href="mes_produits.php">Mes produits</a></li>
        <li><a href="panier.php">Panier <?php echo $nombre_produit_user; ?></a></li>
        <li><a href="liste_discussions.php">Discutions</a></li>

        <?php 
            }
                if($_SESSION['id']) { 

                    $select_username = $bdd->prepare('SELECT * FROM users WHERE id = ?');
                    $select_username->execute(array($_SESSION['id']));
                    $user = $select_username->fetch();?>

        <li><img src="images_static/connexion.png"
                style="margin-left: -2%; padding-right: 0; margin-top: 0.5%; position: absolute; height: 25px; width: 42px;"><a
                href="index.php?deco"><?php echo $user['username']; ?></a>
        </li>
        <?php
                } else { ?>
        <li><a href="connexion.php">Connexion</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <?php
                } ?>
    </ul>
</div>
<div>
    <form method="POST" enctype="multipart/form-data">
        <input type="text" placeholder="Nom du produit" name="nom_produit">
        <input type="text" placeholder="Description du produit" name="desc_produit">
        <input type="text" placeholder="Prix du produit" name="prix_produit">
        <input type="file" name="image[]" />
        <input type="submit" value="Envoyer ma demande" name="valider">
    </form>
</div>
<?php

} else {
    header('Location: index.php');
}

?>