# TP Infra Léo

Projet de création d'un site marchand hébergé sur un serveur OVH.
Location d'un VPS
Location d'un nom de domaine "sisilamarketplace.site"

## Installation Apache

```sudo apt-get update && sudo apt-get upgrade```

```sudo apt-get install apache2```

## Création users
Première connexion admin :
```
ubuntu@vps-6b155bd7:~$ sudo add user marty
ubuntu@vps-6b155bd7:~$ sudo add user louis
ubuntu@vps-6b155bd7:~$ sudo add user mathias
ubuntu@vps-6b155bd7:~$ sudo add user lucas
ubuntu@vps-6b155bd7:~$ sudo password ubuntu
newpasswd XXXXXXXX
```
Donner les droits sudo aux nouveaux users :
```
ubuntu@vps-6b155bd7:~$ visudo
marty ALL=(ALL:ALL) ALL
louis ALL=(ALL:ALL) ALL
lucas ALL=(ALL:ALL) ALL
mathias ALL=(ALL:ALL) ALL
```
Création nouveau groupe :
```
ubuntu@vps-6b155bd7:~$ sudo groupadd admin_users
ubuntu@vps-6b155bd7:~$ sudo adduser marty admin_users
ubuntu@vps-6b155bd7:~$ sudo adduser louis admin_users
ubuntu@vps-6b155bd7:~$ sudo adduser lucas admin_users
ubuntu@vps-6b155bd7:~$ sudo adduser mathias admin_users
```


## Installation PHP

```sudo apt install php libapache2-mod-php```

```sudo systemctl restart apache2```
```
ubuntu@vps-6b155bd7:~$ php -version
PHP 8.0.8 (cli) (built: Oct 26 2021 11:42:42) ( NTS )
```
PHP est bien installé.

## Activation Pare-feu
Nous allons activer le pare-feu afin d'avoir une sécurité accrue sur notre serveur mais avant de l'activer nous allons autoriser le port 22 (pour ssh) et le port 19999 (en vue de l'installation de netdata).
```
sudo ufw allow 22/tcp
Rules updated
Rules updated (v6)
```
```
sudo ufw allow 19999/tcp
Rules updated
Rules updated (v6)
```
```
ubuntu@vps-6b155bd7:~$ sudo ufw enable
Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
Firewall is active and enabled on system startup
ubuntu@vps-6b155bd7:~$ ufw default deny incoming
ubuntu@vps-6b155bd7:~$ sudo ufw reload
Firewall reloaded
ubuntu@vps-6b155bd7:~$ sudo ufw status
Status: active
```
Le pare-feu est bien actif.

Ajout du port 80 (port par défaut du site) :
```
ubuntu@vps-6b155bd7:~$ sudo ufw allow 80
ubuntu@vps-6b155bd7:~$ sudo ufw reload
Firewall reloaded
```

## Sécurisation

Changement de port SSH (réduire nombre de brute force) :
```
marty@vps-6b155bd7:~$ sudo ufw allow 1234
marty@vps-6b155bd7:~$ sudo nano /etc/ssh/ssh_config
--> port 1234
marty@vps-6b155bd7:~$ sudo ufw deny 22
marty@vps-6b155bd7:~$ systemctl restart ssh
```

Désactivation de la connexion root :
```
marty@vps-6b155bd7:~$ sudo nano /etc/ssh/sshd_config
PermitLoginRoot : NO
```

Connexion par clé RSA :
(sur serveur)
```
marty@vps-6b155bd7:~$ mkdir .ssh
```
(sur PC)
```
PS C:\Users\marty> cat ~/.ssh/id_rsa.pub | ssh marty@51.254.124.63 -p 1234 "cat >> ~/.ssh/authorized_keys"
```

Suppression de la connexion par mot de passe : 
```
marty@vps-6b155bd7:~$ sudo nano /etc/ssh/sshd_config
PasswordAuthentification : NO
```

Restreindre l'accès SSH aux users nécessaires : 
```
marty@vps-6b155bd7:~$ sudo nano /etc/ssh/sshd_config
AllowUsers marty mathias lucas louis cowrie
marty@vps-6b155bd7:~$ systemctl restart ssh
```

Mise à jour régulière du serveur : 
```
marty@vps-6b155bd7:~$ sudo apt update
marty@vps-6b155bd7:~$ sudo apt upgrade
```


## Installation Netdata (Monitoring)

```bash <(curl -Ss https://my-netdata.io/kickstart.sh)```

``` 
sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/lib/systemd/system/netdata.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-01-31 09:24:47 UTC; 1min 44s ago
```
Netdata est bien installé.
### Conf Netdata
Nous allons maintenant le configurer en mettant notre adresse ip et en changeant le port. 
```
[global]
    run as user = netdata

    # the default database size - 1 hour
    history = 3600

    # some defaults to run netdata with least priority
    process scheduling policy = idle
    OOM score = 1000

[web]
    web files owner = root
    web files group = netdata
    default port = 39999
    # by default do not expose the netdata port
    bind to = 51.254.124.63
```
Il faut autoriser le port 39999 sur le pare-feu et interdire le port 19999. 

Nous allons faire en sorte que netdata envoie un message d'alerte sur notre serveur discord en cas d'utilisation trop importante du cpu.
Il faut avant tout créer un webhook sur notre serveur discord et le paramétrer, nous réutiliserons son lien dans la configuration des alarmes netdata, nous appellerons le webhook "alarms".
```louis@vps-6b155bd7:/etc/netdata$sudo ./edit-config health_alarm_notify.conf```
```
[...]
#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/938016292756140092/DjUdbRX0n4MznBqekFzMqy1k3G1MEhHiFEGQQBXUn8XXo6Q5BVtoZ_K7Rb4yoYZAqHUyala"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

#------------------------------------------------------------------------------
[...]
```

## Fail2ban

## Portsentry

Installation Portsentry
```
marty@vps-6b155bd7:~$ apt install portsentry
```

IP à whitelister : 
```
marty@vps-6b155bd7:~$ sudo nano /etc/portsentry/portsentry.ignore.static
xx.xxx.xxx.xx
xx.xxx.xxx.xx
xxx.xxx.xxx.xx
xxx.xx.xx.xxx
```

configuration : 
```
marty@vps-6b155bd7:~$ sudo nano /etc/default/portsentry
TCP_MODE="atcp"
UDP_MODE="audp"
marty@vps-6b155bd7:~$ sudo nano /etc/portsentry/portsentry.conf
BLOCK_UDP="1"
BLOCK_TCP="1"
```

*On commente tout les KILL_ROUTE sauf KILL_ROUTE="/sbin/iptables -I INPUT -s $$$target$$$ -j DROP"*
```
marty@vps-6b155bd7:~$ systemctl restart portsentry
```

Vérification des logs : 
```
marty@vps-6b155bd7:~$ grep attackalert /var/log/syslog
Feb  8 14:18:31 vps-6b155bd7 portsentry[45711]: attackalert: UDP scan from host: 80.82.78.100/80.82.78.100 to UDP port: 80
Feb  8 14:18:31 vps-6b155bd7 portsentry[45711]: attackalert: Host 80.82.78.100 has been blocked via wrappers with string: "ALL: 80.82.78.100 : DENY"
Feb  8 14:18:31 vps-6b155bd7 portsentry[45711]: attackalert: Host 80.82.78.100 has been blocked via dropped route using command: "/sbin/iptables -I INPUT -s 80.82.78.100 -j DROP"
```

Voir les IP ban : 
```
marty@vps-6b155bd7:~$ cat /etc/hosts.deny
ALL: 183.136.225.9 : DENY
ALL: 138.246.253.15 : DENY
ALL: 80.82.78.100 : DENY
[...]
```

## HoneyPot (cowrie)

installation des dépendances de cowrie : 
```
marty@vps-6b155bd7:~$ sudo apt-get install git python-virtualenv libssl-dev build-essential libpython-dev python2.7-minimal authbind
```

nouveau user cowrie : 
```
marty@vps-6b155bd7:~$ sudo adduser --disabled-password cowrie
```

installation du code pour cowrie : 
```
marty@vps-6b155bd7:~$ su -cowrie
cowrie@vps-6b155bd7:~$ git clone http://github.com/micheloosterhof/cowrie
```

création de l'environnement virutel pour cowrie et python : 
```
cowrie@vps-6b155bd7:~$ cd cowrie/
cowrie@vps-6b155bd7:~/cowrie$ virtualenv cowrie-env
cowrie@vps-6b155bd7:~/cowrie$ source cowrie-env/bin/activate
```

installation des paquets python necéssaires au démarrage de cowrie : 
```
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie$ pip install --upgrade pip
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie$ pip install --upgrade -r requirements.txt
```

configuration de cowrie : 
```
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie$ cd etc/ 
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie/etc$ cp cowrie.cfg.dist cowrie.cfg
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie/etc$ nano cowrie.cfg
[telnet]
enabled = true
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie$ bin/cowrie start
```

Voir les logs cowrie : 
```
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie$ cd var/log/cowrie/
(cowrie-env) cowrie@vps-6b155bd7:~/cowrie/var/log/cowrie$ cat cowrie.log
```

## Nom de domaine
Le nom de domaine loué sur OVH (à savoir "sisilamarketplace.site") a été relié à l'adresse IP du site directement depuis le site d'OVH

## HTTPS (Certificat SSL)

Utilisation de cerbot pour avoir des certificat SSL

Installation de cerbot :
```
louis@vps-6b155bd7:~/louis$ sudo snap install --classic certbot
```
Suite de l'instalation (avec conf automatique de apache)
```
louis@vps-6b155bd7:~/louis$ sudo certbot --apache
``` 
Autorisation du port "443" (HTTPS) :
```louis@vps-6b155bd7:~/louis$ sudo ufw allow 443```

## Le site et ses fonctionnalités
Avoir un serveur bien configuré c'est bien mais avoir quelque chose dessus c'est mieux ! Dans le cadre de notre projet nous avons décidé de créer un site en PHP.



Page inscription : 

    - Formulaire envoyé en POST
    - Plusieurs vérifications comme : si le username est pas déjà utilisé
    - Une insertion dans la bdd des informations (base de donneés)
    - le mot de passe est sécurisé grace au bcrypt
    
    
    
Page connexion : 

    - Formulaire envoyé en POST
    - Plusieurs vérifications comme : 
    
        - Vérifier que les inputs soient pas vide et ne contiennent pas de code
        - Vérifier que le username entré existe dans la bdd
        - Vérifier que le mot de passe est correct et corresponde au username
        
        - Mise en place du SESSION ID
        
        
        
Page de discussions : 

    - Récuperation des données en GET
    - Déchifrage ou chiffrage des messages
    - Envoi de message qui se fait en POST
    - Insertion dans la bdd des messages envoyés
    - Possibilité de bloquer un utilisateur / débloquer
    
    
    
Page de mise en ligne de produits : 

    - Formulaire en POST
    - Ajout de photos (png, jpeg, etc...)
    - Insertion du produit dans la bdd ainsi que de l'image dans le dossier image
    
    
    
Page d'accueil : 

    - Avec requete GET : 
    
        - Ajout au panier par requete POST
        
    - Sans requete GET :
    
        - Affichage des produits qui ont été valider par un administrateur
        


Page panier : 

    - Récuperation des produits dans le panier avec la bdd avec la methode POST 
    - Suppression de produits avec la method GET puis suppression dans la bdd
    
    
    
Espace administrateur : 

    Sans requete GET :
        
        - Affichage de connexion 
        - Vérification de l'utilisateur + verification si administrateur dans la bdd
        
    Avec requete GET :
    
        - Affichage du panel admin
        - Posibilité de supprimer un produit